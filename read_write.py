import os
import sys
import datetime
from ruamel.yaml import YAML

yaml = YAML(typ = "safe", pure = True)
date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

def read_yaml(yaml_file):
    with open(yaml_file, 'r') as file:
        return yaml.load(file)


def write_yaml(yaml_file, key, value):
    data = read_yaml(yaml_file)
    data[key] = value
    data['meta']['last-edit'] = date_now
    with open(yaml_file, 'w') as file:
        yaml.dump(data, file)


def create_yaml_file(yaml_file):
    data = {'meta': {'created': date_now, 'id': 0, 'last-edit': ''}, 'note': ''}
    with open(yaml_file, 'w') as yaml_file:
        yaml.dump(data, yaml_file)
