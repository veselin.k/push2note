import sys
from gui import TreeWidget, TextEdit
from PyQt5.QtWidgets import QApplication, QTreeWidgetItem, QDesktopWidget, \
        QMainWindow, QSplitter, QVBoxLayout, QAction
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QRect
from find_replace import FindReplace
import config_app

class Push2Note(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'Push2Note'
        self.setWindowTitle(self.title)

        self.splitter = QSplitter()
        self.setCentralWidget(self.splitter)
        vbox = QVBoxLayout(self.splitter)
        self.text_field = TextEdit()
        self.tree_widget = TreeWidget(self.text_field)
        self.text_field.treeWidget = self.tree_widget
        vbox.addWidget(self.tree_widget)
        vbox.addWidget(self.text_field)
        self._create_menu()
        self.tree_widget.currentItemChanged.connect(self.statusbar_item_name)
        self.show()

    def statusbar_item_name(self):
        if self.tree_widget.currentItem():
            self.statusBar().showMessage(self.tree_widget.currentItem().text(0))
        else:
            self.statusBar().showMessage('')

    def _create_menu(self):
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('File')
        edit_menu = main_menu.addMenu('Edit')
        view_menu = main_menu.addMenu('View')
        view_menu = main_menu.addMenu('Help')

        exit_button = QAction(QIcon('exit24.png'), 'Exit', self)
        exit_button.setShortcut('Ctrl+Q')
        exit_button.setStatusTip('Exit application')
        exit_button.triggered.connect(self.close)
        file_menu.addAction(exit_button)

        search_button = QAction('Find', self)
        search_button.setShortcut('Ctrl+F')
        edit_menu.addAction(search_button)
        search_button.triggered.connect(self.find_in_note)

    def find_in_note(self):
        self.find_form = FindReplace(self.text_field)
        self.find_form.show()

    def close_event(self, event):
        print("CLOSE")


def main():
    app = QApplication(sys.argv)
    ex = Push2Note()
    ex.resize(int(config_app.WIDTH), int(config_app.HEIGHT))

    rect = ex.frameGeometry()
    res_cent = QDesktopWidget().availableGeometry().center()
    rect.moveCenter(res_cent)
    ex.move(rect.topLeft())

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
