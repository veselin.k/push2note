import os
import sys
import shutil
from PyQt5.QtWidgets import QApplication, QWidget, \
        QTreeWidget, QSizePolicy, QStackedWidget, \
        QTreeWidgetItem, QMenu, QInputDialog, QLineEdit, QTreeWidgetItemIterator
from PyQt5.QtGui import QColor, QFont
from PyQt5.QtCore import Qt, QEvent
from PyQt5.Qsci import QsciScintilla
from helpers import  dir_list
from read_write import read_yaml, write_yaml, create_yaml_file
import config_app

notes_dir = 'notes'

class TreeWidget(QTreeWidget):
    def __init__(self, textEdit, parent=None):
        QTreeWidget.__init__(self, parent=parent)
        self.textEdit = textEdit
        self.headerItem().setText(0, "Notes")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(25)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.redraw_tree()
        self.currentItemChanged.connect(self.show_item_content)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.tree_menu)


    def redraw_tree(self):
        self.clear()
        for note in dir_list(notes_dir):
            node = QTreeWidgetItem()
            self.addTopLevelItem(node)
            node.setText(0, note)
            node.setFlags(node.flags() | Qt.ItemIsTristate | Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            for subnote in dir_list(os.path.join(notes_dir, note)):
                child = QTreeWidgetItem(node)
                child.setText(0, subnote)
                node.setFlags(node.flags() | Qt.ItemIsTristate | Qt.ItemIsSelectable | Qt.ItemIsEnabled)


    def show_item_content(self, item):
        if item is None:
            return

        if item.parent():
            sub_note_path = "{}/{}/{}/{}.yaml".format(notes_dir, item.parent().text(0), item.text(0), item.text(0))
            self.textEdit.setText(read_yaml(sub_note_path)['note'])
        else:
            note_path = "{}/{}/{}.yaml".format(notes_dir, item.text(0), item.text(0))
            self.textEdit.setText(read_yaml(note_path)['note'])


    def input_note_name(self, textToShow):
        text, ok = QInputDialog.getText(self, 'Name ', 'Note Name: ', QLineEdit.Normal, textToShow)
        if ok and text != '':
            input_notename = text
            return input_notename
        else:
            pass


    def create_note(self, note_name, sub_note_name=None):
        if sub_note_name is None:
            note_path = os.path.join(notes_dir, note_name)
            yaml_file = "{}.{}".format(note_name, 'yaml')
        else:
            if self.currentItem().parent():
                note_name = self.currentItem().parent().text(0)
            else:
                note_name = self.currentItem().text(0)
            note_path = os.path.join(notes_dir, note_name, sub_note_name)
            yaml_file = "{}.{}".format(sub_note_name, 'yaml')
        try:
            os.makedirs(os.path.join(note_path))
            create_yaml_file(os.path.join(note_path, yaml_file))

            self.redraw_tree()
            self.iterator(note_path.split('/')[-1])
        except OSError:
            pass


    def delete_note(self):
        if self.currentItem().parent():
            shutil.rmtree(os.path.join(notes_dir,
                self.currentItem().parent().text(0), self.currentItem().text(0)))
        else:
            shutil.rmtree(os.path.join(notes_dir, self.currentItem().text(0)))
        self.redraw_tree()


    def iterator(self, item_name_string):
        iter = QTreeWidgetItemIterator(self)
        while iter.value():
            item = iter.value()
            if item.text(0) == item_name_string:
                self.textEdit.clear()
                self.setCurrentItem(item)
            iter += 1


    def rename_note(self):
        new_name = self.input_note_name(self.currentItem().text(0))
        try:
            if self.currentItem().parent():
                note_path = os.path.join(notes_dir, self.currentItem().parent().text(0), self.currentItem().text(0))
                note_yaml_file = os.path.join(note_path, "{}.{}".format(self.currentItem().text(0), 'yaml'))
                note_yaml_new_name = os.path.join(note_path, "{}.{}".format(new_name, 'yaml'))
                note_folder_new_name = os.path.join(notes_dir, self.currentItem().parent().text(0), new_name)
                item_str = os.path.join(notes_dir, new_name).split('/')[-1]
            else:
                note_path = os.path.join(notes_dir, self.currentItem().text(0))
                note_yaml_file = os.path.join(note_path, "{}.{}".format(self.currentItem().text(0), 'yaml'))
                note_yaml_new_name = os.path.join(note_path, "{}.{}".format(new_name, 'yaml'))
                note_folder_new_name = os.path.join(notes_dir, new_name)
                item_str = os.path.join(notes_dir, new_name).split('/')[-1]

            os.rename(note_yaml_file, note_yaml_new_name)
            shutil.move(note_path, note_folder_new_name)
            self.redraw_tree()
            self.iterator(item_str)
        except TypeError:
            pass


    def tree_menu(self, pos):
        menu = QMenu()
        addNoteAction = menu.addAction("Add Note")
        addSubNoteAction = menu.addAction("Add SubNote")
        renameNoteAction = menu.addAction("Rename")
        deleteNoteAction = menu.addAction("Delete")
        action = menu.exec_(self.mapToGlobal(pos))

        if action == addNoteAction:
            try:
                self.create_note(self.input_note_name(''))
            except TypeError:
                pass
        if action == addSubNoteAction:
            try:
                self.create_note(self.currentItem().text(0), self.input_note_name(''))
            except AttributeError:
                pass
        if action == renameNoteAction:
            self.rename_note()
        if action == deleteNoteAction:
            self.delete_note()


class TextEdit(QsciScintilla):
    def __init__(self, parent=None):
        QsciScintilla.__init__(self, parent=parent)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(60)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.sci_config()
        self.SCN_FOCUSOUT.connect(self.write_note)


    def sci_config(self):
        self.setFont(QFont(config_app.FONT_FAMILLY, int(config_app.FONT_SIZE)))
        self.setCaretLineVisible(True)
        self.setCaretLineBackgroundColor(QColor(config_app.CARETLINEBACKGROUNDCOLOR))
        self.setMarginWidth(1, 20)
        self.setMarginsBackgroundColor(QColor("#333333"))
        self.setMarginsForegroundColor(QColor("#CCCCCC"))
        self.markerDefine(QsciScintilla.RightTriangle)
        self.setMarkerBackgroundColor(QColor("#ACF39D"))


    def write_note(self):
        item = self.treeWidget.currentItem()
        if item.parent():
            sub_note_file = "{}/{}/{}/{}.yaml".format(notes_dir, item.parent().text(0), item.text(0), item.text(0))
            write_yaml(sub_note_file, 'note', self.text())
        else:
            note_file = "{}/{}/{}.yaml".format(notes_dir, item.text(0), item.text(0))
            write_yaml(note_file, 'note', self.text())
            