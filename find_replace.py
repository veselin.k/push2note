import sys
from PyQt5.QtWidgets import QDialog, QApplication, QTabWidget, QWidget, \
        QVBoxLayout, QHBoxLayout, QLineEdit, QPushButton, QLabel, QDesktopWidget
from gui import TextEdit


class FindReplace(QDialog):
    def __init__(self, textEdit):
        super().__init__()

        self.textEdit = textEdit

        self.setWindowTitle('Find / Replace')

        tab_widget = QTabWidget()
        tab_widget.addTab(FindTab(textEdit), "Find")
        tab_widget.addTab(ReplaceTab(), "Replace")

        vbox = QVBoxLayout()
        vbox.addWidget(tab_widget)

        self.setLayout(vbox)


class FindTab(QWidget):
    def __init__(self, textEdit):
        super().__init__()

        self.textEdit = textEdit

        self.find_label = QLabel('Find:')
        self.find_field = QLineEdit(self)
        self.find_up_button = QPushButton('Find up', self)
        self.find_down_button = QPushButton('Find down', self)
        self.select_all_button = QPushButton('Select all', self)

        text_field_hbox_layout = QHBoxLayout()
        text_field_hbox_layout.addWidget(self.find_label)
        text_field_hbox_layout.addWidget(self.find_field)

        buttons_tab_vbox_layout = QHBoxLayout()
        buttons_tab_vbox_layout.addWidget(self.find_up_button)
        buttons_tab_vbox_layout.addWidget(self.find_down_button)
        buttons_tab_vbox_layout.addWidget(self.select_all_button)

        find_tab_vbox_layout = QVBoxLayout()
        find_tab_vbox_layout.addLayout(text_field_hbox_layout)
        find_tab_vbox_layout.addLayout(buttons_tab_vbox_layout)

        self.setLayout(find_tab_vbox_layout)

        self.find_down_button.clicked.connect(self.find_down)


    def find_down(self):
        searchString = self.find_field.text()
        cursorPosition = self.textEdit.getCursorPosition()
        for line in self.textEdit.text():
            print(line)
#            for word in line.split(' '):
#                print(word)

#        print(searchString)


class ReplaceTab(QWidget):
    def __init__(self):
        super().__init__()


def main():
    app = QApplication(sys.argv)
    tabwidget = FindReplace()
    tabwidget.resize(400, 200)
    rect = tabwidget.frameGeometry()
    res_cent = QDesktopWidget().availableGeometry().center()
    rect.moveCenter(res_cent)
    tabwidget.move(rect.topLeft())


if __name__ == '__main__':
    main()

