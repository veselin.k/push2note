from cfg_parser import Config

CONFIG_FILE = 'push2note.conf'
CFG_VAL = Config()
WIDTH = CFG_VAL.readCfgValue(CONFIG_FILE, 'WINDOWSIZE', 'Width')
HEIGHT = CFG_VAL.readCfgValue(CONFIG_FILE, 'WINDOWSIZE', 'Height')
FONT_FAMILLY = CFG_VAL.readCfgValue(CONFIG_FILE, 'FONT', 'Familly')
FONT_SIZE = CFG_VAL.readCfgValue(CONFIG_FILE, 'FONT', 'Size')
CARETLINEBACKGROUNDCOLOR = CFG_VAL.readCfgValue(CONFIG_FILE, 'COLORS', 'CaretLineBackgroundColor')
