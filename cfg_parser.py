import configparser
class Config():
    def __init__(self):
        self.parser = configparser.RawConfigParser()


    def readCfgSection(self, file, section):
        self.parser.read(file)
        section_dict = {}
        options = self.parser.options(section)
        for option in options:
            section_dict[option] = self.parser.get(section, option)
        return section_dict


    def readCfgValue(self, file, section, valueName):
        self.parser.read(file)
        return self.parser[section][valueName]


    def writeCfgList(self, file, section, valuesList):
        self.parser[section] = {}
        cfgMainSection = self.parser[section]
        for value in valuesList:
            cfgMainSection[value[0]] = value[1]
        with open(file, 'w') as configfile:
            self.parser.write(configfile)


    def writeCfgUpdateValue(self, file, section, valueName, valueKey):
        self.parser.read(file)
        self.parser[section][valueName] = valueKey
        with open(file, 'w') as configfile:
            self.parser.write(configfile)

