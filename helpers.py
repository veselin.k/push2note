import os

def dir_list(directory):
    return [folder for folder in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, folder))]
